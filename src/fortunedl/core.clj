(ns fortunedl.core
  (:gen-class)
  (:require
   [clojure.java.io :as io]
   [jsonista.core :as j]))

(def fs "https://a.4cdn.org/%s/thread/%s.json")
(def mapper (j/object-mapper {:decode-key-fn true}))

(defn board [ss]
  (nth ss 3))

(defn thread [ss]
  (nth ss 5))

(defn thread->cdn [brd th]
  (format fs brd th))

(defn post->file-path
  [board tim ext]
  (str "https://i.4cdn.org/" board "/" tim ext))

(defn make-xf
  [board]
  (comp
   (filter (fn [post] (get post :filename)))
   (map (fn [{:keys [ext filename tim]}]
          {:uri (post->file-path board tim ext)
           :filename (str filename ext)}))))

(defn copy!
  [uri file]
  (with-open [in (io/input-stream uri)
              out (io/output-stream file)]
    (io/copy in out)))

(defn -main
  [& args]
  (doseq [arg args]
    (let [ss (clojure.string/split arg #"/")
          brd (board ss)
          th (thread ss)
          cdn (thread->cdn brd th)
          th-json (j/read-value (slurp cdn) mapper)
          posts (get th-json :posts)
          xf (make-xf brd)
          payload (transduce xf conj posts)]
      (doseq [{:keys [uri filename]} payload]
        (println 'Downloading uri '-> filename)
        (copy! uri filename)))))
