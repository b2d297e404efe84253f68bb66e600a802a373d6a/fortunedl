# fortunedl

Small script to download files from 4chan with correct filenames.

## Requirements

- jvm
- lein

## Installation

Clone this repo

run:
```
lein uberjar
```

## Usage

    $ java -jar fortunedl-0.1.0-standalone.jar [args]

## Single Executable

run:

```sh
cat ./script/stub.sh target/uberjar/fortunedl-0.1.0-SNAPSHOT-standalone.jar > 4dl && chmod +x 4dl
```

## Native Image

If for some reason you care about Clojure's slow start up time you can
create a native image using graalvm:

- Download latest release: https://github.com/oracle/graal/releases
- Extract to a directory of your choice
- export the extracted directory to be `GRAALVM_HOME`
- run `./script/compile`

The resulting native image is a bit heavy due to containing BouncyCastle
As I didn't know how to make it work with the Sun encryption library.
If you know how to, let me know.

## License

Copyright © 2019 anon

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
